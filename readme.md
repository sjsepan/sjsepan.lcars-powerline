# sjsepan.lcars-powerline TOML theme for Bash using Starship and NerdFonts on Linux Mint. Also tested on fish and PowerShell on Linux, and fish and bash on GhostBSd.

The perfect (console-theme) gift for that Star Trek fan(atic) in your life :-)
Colors sampled from the Classic and TNG Legacy themes on <https://www.thelcars.com/> .
Customization based on pastel-powerline preset.
Edited using VSCode and Better TOML extension.

![sjsepan.lcars-powerline.png](./sjsepan.lcars-powerline.png?raw=true "Screenshot")

## Starship home

<https://starship.rs/>

## NerdFonts home

<https://www.nerdfonts.com/>

## TOML Home

Tom's Obvious Minimal Language

<https://toml.io/en/>

## Install Starship

`curl -O https://starship.rs/install.sh`

`chmod +x install.sh`

`mkdir ~/.local/share/applications/starship`

`./install.sh -b ~/.local/share/applications/starship`

`export PATH="~/.local/share/applications/starship:$PATH"`

`eval "$(starship init bash)"`

## Configure Linux Mint

`cd ~`

`xed .bashrc`

Add lines to end:

`export PATH="~/.local/share/applications/starship:$PATH"`

`eval "$(starship init bash)"`

## Example .toml from Preset

`starship preset pastel-powerline > ~/.config/starship.pastel-powerline.toml`

## NerdFonts Cheat-Sheet

<https://www.nerdfonts.com/cheat-sheet>

## Edit

Make changes to ~/.config/starship.toml; to test re-execute:

`eval "$(starship init bash)"`

## History
0.8:
~NerdFix replacements
~foreground tweaks
~FreeDSB compatibility
~SHELL, ENV and OS modules


0.7:
~add support for starship 1.15.0 and the NerdFonts 3.0 which it supports; handle broken clock ($time) icon due to NF3 changes

0.6:
~Add support for SSH by displaying remote hostname in prompt when connected.

0.5:
~Modify format of prompt to show dotnet symbol in blue FG.
~Fix continuation prompt.

0.4:
~changed curve to simple bar, with Status; Check for success, X and message for Failure.

0.3:
~backed-out extra leading row, emulated simpler curve in color band

0.2:
~added some blue
~increased rows involved and emulated curve in color band

0.1:
~initial release of theme

Stephen J Sepan

sjsepan@yahoo.com

8-28-2023
